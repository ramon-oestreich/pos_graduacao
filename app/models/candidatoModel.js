var db = require('../../config/database');
module.exports = function(){
    this.all = function(retorno){
        var conexao = db();
    return conexao.query('SELECT candidatos.* ,sum(nota) as notas from candidatos  join pontuacoes on candidatos.id = pontuacoes.candidatos_id group by candidatos.id order by notas desc;',retorno)
       
    }

    this.allUser = function(retorno){
        var conexao = db();
    return conexao.query('SELECT * from candidatos',retorno)
       
    }

    this.allNote = function(retorno){
        var conexao = db();
    return conexao.query('select * from avaliadores',retorno)
       
    }


    //select * from candidatos where id = ?
   
    this.find = function(id,retorno){
        var conexao = db();
    return conexao.query('select candidatos.*, sum(nota) as notas from candidatos join pontuacoes on candidatos.id = pontuacoes.candidatos_id where candidatos_id = ?',id,retorno)
       
    }

    this.save = function(dados,retorno){
        var conexao = db();
        console.log(dados)
        if(dados.tipo ==="0"){
            console.log('avaliador caso')
            return conexao.query('insert into avaliadores set ?',dados,retorno)
        }else{
            console.log('candidato caso')
            return conexao.query('insert into candidatos set ?',dados,retorno)
        }
    }

    this.saveNote = function(dados,retorno){
        var conexao = db();
        return conexao.query('insert into pontuacoes set ?',dados,retorno)
    }

    return this

}