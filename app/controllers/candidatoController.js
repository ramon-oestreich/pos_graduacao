var candidatoModel = require('../models/candidatoModel')();

module.exports.index = function(req,res){
    candidatoModel.all(function(err,resultado){
        res.render('candidatos/listagem',{candidatos:resultado,erros:{},dados:{}})
       })
}

module.exports.indexUser = function(req,res){
    candidatoModel.allUser(function(err,resultado){
        res.render('candidatos/usuario',{candidatos:resultado,erros:{},dados:{}})
       })
}



module.exports.store = function(req,res){
      var dados = req.body;
    //req.assert('nome','Preencha um nome').notEmpty();
    //req.assert('email','Preencha com um email').isEmail();

    var erros = req.validationErrors();
    if(erros){
        console.log(erros);
        candidatoModel.allUser(function(err,resultado){
            res.render('candidatos/usuario',{candidatos:resultado,erros:erros,dados:dados})
           })
        return
    }
    candidatoModel.save(dados,function(err,resultado){
      
        if(!err){

            res.redirect('/')
            }else{
                console.log(err)
                console.log('erro ao adicionar')
                res.redirect('/')
            }
       })
}




module.exports.indexNote = function(req,res){
    candidatoModel.allNote(function(err,resultado){
        res.render('candidatos/nota',{avaliadores:resultado,erros:{},dados:{}})
        
       })
}

module.exports.storeNote = function(req,res){
    var dados = req.body;
    //if(req.body.avaliadores_id === )
    console.log(req.body)
   // console.log(res)
    if(!req.body.avaliadores_id){
        console.log(true)
    }
    console.log('entrou no save da nota')
  var erros = req.validationErrors();
  if(erros){
      console.log(erros);
      candidatoModel.all(function(err,resultado){
          res.render('candidatos/nota',{avaliadores:resultado,erros:erros,dados:dados})
         })
      return
  }
  candidatoModel.saveNote(dados,function(err,resultado){
    
      if(!err){
        console.log('entrou no sem erro')
          res.redirect('/nota')
          }else{
              console.log('erro ao adicionar')
              res.redirect('/nota')
          }
     })
}



module.exports.show = function(req,res){
   candidatoModel.find(req.params.id,function(err,resultado){
       console.log(resultado)
       console.log('request '+req.params.id)
       console.log('resposta '+res.params)
    if(resultado[0] && !err){
    res.render('candidatos/home',{candidato:resultado[0]})
    }else{
        console.log('esse candidato nao existe pomba')
        res.redirect('/')
    }
   }) 
}