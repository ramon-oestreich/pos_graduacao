var clienteModel = require('../models/clienteModel')();

module.exports.index = function(req,res){
    clienteModel.all(function(err,resultado){
        res.render('site/show',{clientes:resultado,erros:{},dados:{}})
       })
}

module.exports.store = function(req,res){
    var dados = req.body;
    req.assert('nome','Preencha um nome').notEmpty();
    req.assert('email','Preencha com um email').isEmail();

    var erros = req.validationErrors();
    if(erros){
        console.log(erros);
        clienteModel.all(function(err,resultado){
            res.render('site/home',{clientes:resultado,erros:erros,dados:dados})
           })
        return
    }
    clienteModel.save(dados,function(err,resultado){
        if(!err){

            res.redirect('/')
            }else{
                console.log('erro ao adicionar')
                res.redirect('/')
            }
       })
}

module.exports.show = function(req,res){
   clienteModel.find(req.params.id,function(err,resultado){
    if(resultado[0] && !err){
    res.render('site/detalhe',{cliente:resultado[0]})
    }else{
        console.log('esse cliente nao existe')
        res.redirect('/')
    }
   }) 
}